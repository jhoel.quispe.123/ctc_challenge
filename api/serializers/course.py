from rest_framework import serializers
from app.courses.models import Course


class CourseListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Course
        fields = ['title']
