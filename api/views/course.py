from rest_framework.views import APIView

from rest_framework import generics


from app.courses.models import Course
from api.serializers.course import CourseListSerializer


class CourseListView(generics.ListAPIView):

    serializer_class = CourseListSerializer
    queryset = Course.objects.all()


class JuniorCourseListView(generics.ListAPIView):

    serializer_class = CourseListSerializer
    queryset = Course.junior_objects.all()
