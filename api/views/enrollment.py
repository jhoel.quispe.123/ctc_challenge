from rest_framework.views import APIView

from rest_framework import generics


from app.courses.models import Enrollment
from api.serializers.enrollment import EnrollmentSerializer


class EnrollmentView(generics.ListCreateAPIView):

    serializer_class = EnrollmentSerializer
    queryset = Enrollment.objects.all()
