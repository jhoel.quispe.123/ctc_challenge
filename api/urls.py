from django.urls import include, path

from api.views import course as course_views
from api.views import enrollment as enrollment_views


course_urls = [
    path('',
         course_views.CourseListView.as_view(),
         name='courses_list'),
    path('junior/', course_views.JuniorCourseListView.as_view(),
         name='junior_courses_list'),
]

enrollment_urls = [
    path('',
         enrollment_views.EnrollmentView.as_view(),
         name='enrollment_list_add'),
]

urlpatterns = [
    path('courses/', include(course_urls)),
    path('enrollment/', include(enrollment_urls)),
]
