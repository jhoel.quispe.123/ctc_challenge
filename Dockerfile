# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY ./requirements.txt ./requirements-dev.txt /code/
RUN pip install -r requirements.txt -r requirements-dev.txt
COPY . /code/


