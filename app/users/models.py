from django.db import models
from django.conf import settings

from app.common.models import BaseModel


class Parent(BaseModel):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, related_name="parent", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.user


class Student(BaseModel):
    parent = models.ForeignKey(Parent, related_name="sons", on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    birthday = models.DateField()

    def __str__(self):
        return self.name
