from pyexpat import model
from django.db import models


class BaseModel(models.Model):

    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    modified = models.DateTimeField(auto_now=True, editable=False, blank=True)
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True
