from django.db import models

from app.common.models import BaseModel
from app.users.models import Student
from .course import Course


class Enrollment(BaseModel):
    course = models.ForeignKey(
        Course, related_name="enrollments", on_delete=models.CASCADE
    )
    student = models.ForeignKey(
        Student, related_name="enrollments", on_delete=models.CASCADE
    )
    join_url = models.URLField(max_length=400, null=True, blank=True)
