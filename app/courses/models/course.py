from django.db import models

from app.common.models import BaseModel


COURSE_JUNIOR_AGE = 8


class CourseJuniorManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(max_age__lte=COURSE_JUNIOR_AGE)

    def deactivate_enrollments(self):
        return self.enrollments.update(active=False)


class Course(BaseModel):
    title = models.CharField(max_length=128)
    min_age = models.SmallIntegerField()
    max_age = models.SmallIntegerField()
    objects = models.Manager()
    junior_objects = CourseJuniorManager()

    def __str__(self):
        return self.title
