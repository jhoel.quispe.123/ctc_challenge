# CTC Challenge



## Run Project

1. Build the images:

    ```sh
    $ docker-compose build
    ```
2. Run development project:

    ```sh
    $ docker-compose up
    ```

## Project Structure


```
├── api
│   ├── serializers
│   ├── urls.py
│   └── views
├── app
│   ├── config
│   │   ├── settings.py
│   │   ├── swagger.py
│   │   ├── urls.py
│   ├── courses
│   │   └── models
│   │       ├── course.py
│   │       ├── enrollment.py
│   │       └── __init__.py
│   ├── __init__.py
│   └── users
│       └── models.py

```
